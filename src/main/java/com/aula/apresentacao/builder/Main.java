/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aula.apresentacao.builder;

/**
 *
 * @author Alysson
 */
public class Main {
    public static void main(String[] args) {
        
        Aluno aluno = new Aluno(8.5, 6.5, 7.0, 9.0, 7.8, "Everton Spindola");
        
        System.out.println(aluno);
        
        
        Aluno alunoBuilder = new Aluno.AlunoBuilder("Everton Spindola")
                .compiladoresNota(8.5)
                .engenhariaDeSoftwareNota(6.5)
                .estatisticaNota(7.0)
                .laboratorioDeProgramacaoNota(9.0)
                .bancoDeDadosNota(7.8)
                .build();
        
        System.out.println(alunoBuilder);
    }
}
