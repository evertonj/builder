/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aula.apresentacao.builder;

/**
 *
 * @author Alysson
 */
public class Aluno {

    private double compiladoresNota;
    private double engenhariaDeSoftwareNota;
    private double estatisticaNota;
    private double laboratorioNota;
    private double bancoDeDadosNota;
    private String nome;

    public Aluno(double compiladoresNota, double engenhariaDeSoftwareNota, double estatisticaNota, double laboratorioNota, double bancoDeDadosNota, String nome) {
        this.compiladoresNota = compiladoresNota;
        this.engenhariaDeSoftwareNota = engenhariaDeSoftwareNota;
        this.estatisticaNota = estatisticaNota;
        this.laboratorioNota = laboratorioNota;
        this.bancoDeDadosNota = bancoDeDadosNota;
        this.nome = nome;
    }


    public Aluno(AlunoBuilder builder) {
        this.compiladoresNota = builder.compiladoresNota;
        this.engenhariaDeSoftwareNota = builder.engenhariaDeSoftwareNota;
        this.estatisticaNota = builder.estatisticaNota;
        this.laboratorioNota = builder.laboratorioNota;
        this.bancoDeDadosNota = builder.bancoDeDadosNota;
        this.nome = builder.nome;
    }
    
    public static class AlunoBuilder{
        private double compiladoresNota;
        private double engenhariaDeSoftwareNota;
        private double estatisticaNota;
        private double laboratorioNota;
        private double bancoDeDadosNota;
        private String nome;
        
        public AlunoBuilder(String nome) {
            this.nome = nome;
        }
        

        public AlunoBuilder compiladoresNota(double compiladoresNota) {
            this.compiladoresNota = compiladoresNota;
            return this;
        }

        public AlunoBuilder engenhariaDeSoftwareNota(double engenhariaDeSoftwareNota) {
            this.engenhariaDeSoftwareNota = engenhariaDeSoftwareNota;
            return this;
        }

        public AlunoBuilder estatisticaNota(double estatisticaNota) {
            this.estatisticaNota = estatisticaNota;
            return this;
        }

        public AlunoBuilder laboratorioDeProgramacaoNota(double laboratorioNota) {
            this.laboratorioNota = laboratorioNota;
            return this;
        }

        public AlunoBuilder bancoDeDadosNota(double bancoDeDadosNota) {
            this.bancoDeDadosNota = bancoDeDadosNota;
            return this;
        }
        
        public Aluno build(){
            return new Aluno(this);
        }
    
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getCompiladoresNota() {
        return compiladoresNota;
    }

    public void setCompiladoresNota(double compiladoresNota) {
        this.compiladoresNota = compiladoresNota;
    }

    public double getEngenhariaDeSoftwareNota() {
        return engenhariaDeSoftwareNota;
    }

    public void setEngenhariaDeSoftwareNota(double engenhariaDeSoftwareNota) {
        this.engenhariaDeSoftwareNota = engenhariaDeSoftwareNota;
    }

    public double getEstatisticaNota() {
        return estatisticaNota;
    }

    public void setEstatisticaNota(double estatisticaNota) {
        this.estatisticaNota = estatisticaNota;
    }

    public double getLaboratorioNota() {
        return laboratorioNota;
    }

    public void setLaboratorioNota(double laboratorioNota) {
        this.laboratorioNota = laboratorioNota;
    }

    public double getBancoDeDadosNota() {
        return bancoDeDadosNota;
    }

    public void setBancoDeDadosNota(double bancoDeDadosNota) {
        this.bancoDeDadosNota = bancoDeDadosNota;
    }

    @Override
    public String toString() {
        return "Aluno{nome=" + nome  + ", compiladoresNota=" + compiladoresNota + ", engenhariaDeSoftwareNota=" + engenhariaDeSoftwareNota + ", estatisticaNota=" + estatisticaNota + ", laboratorioNota=" + laboratorioNota + ", bancoDeDadosNota=" + bancoDeDadosNota + '}';
    }

   
}
